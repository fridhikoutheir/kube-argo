package com.kubernetes.argo.kubernetesargoCd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KubernetesArgoCdApplication {

	public static void main(String[] args) {
		SpringApplication.run(KubernetesArgoCdApplication.class, args);
	}

}
