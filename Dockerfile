#
# Build stage
#
FROM maven:3.6.3-jdk-11 as Build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

#
# Package stage
#
FROM openjdk:11-jre-slim
RUN file="$(ls -1 /home)" && echo $file
COPY --from=build /home/app/target/*.jar kubArgo.jar
EXPOSE 8081
ENTRYPOINT ["java","-jar","/kubArgo.jar"]